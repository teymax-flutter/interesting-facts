import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/fact_bloc.dart';
import '../bloc/fact_event.dart';
import '../widgets/widgets.dart';

class RandomScreen extends StatelessWidget {
  static final String name = 'RandomScreen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Random Facts'),
      ),
      drawer: CustomDrawer(
        currentScreen: name,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 10),
            MainDisplay(),
            SizedBox(height: 20),
            Container(
              height: MediaQuery.of(context).size.height / 10,
              child: RaisedButton(
                color: Theme.of(context).accentColor,
                onPressed: () {
                  BlocProvider.of<FactBloc>(context).add(RandomFactLoadEvent());
                },
                textTheme: ButtonTextTheme.primary,
                child: const Text(
                  'Show random fact!',
                  style: TextStyle(fontSize: 25),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
