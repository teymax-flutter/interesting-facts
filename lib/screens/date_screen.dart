import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class DateScreen extends StatelessWidget {
  static final String name = 'DateScreen';
  final String factType = 'date';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Facts about some date'),
      ),
      drawer: CustomDrawer(
        currentScreen: name,
      ),
      body: ScreenBody(factType: factType),
    );
  }
}
