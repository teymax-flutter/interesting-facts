import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class NumberScreen extends StatelessWidget {
  static final String name = 'NumberScreen';
  final String factType = 'trivia';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Number Facts'),
      ),
      drawer: CustomDrawer(
        currentScreen: name,
      ),
      body: ScreenBody(factType: factType),
    );
  }
}
