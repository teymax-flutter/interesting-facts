import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class YearScreen extends StatelessWidget {
  static final String name = 'YearScreen';
  final String factType = 'year';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Facts about some year'),
      ),
      drawer: CustomDrawer(
        currentScreen: name,
      ),
      body: ScreenBody(factType: factType),
    );
  }
}
