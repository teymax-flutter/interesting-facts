import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

class MathScreen extends StatelessWidget {
  static final String name = 'MathScreen';
  final String factType = 'math';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Math Facts'),
      ),
      drawer: CustomDrawer(
        currentScreen: name,
      ),
      body: ScreenBody(factType: factType),
    );
  }
}
