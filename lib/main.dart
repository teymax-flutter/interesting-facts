import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/fact_bloc.dart';
import 'screens/date_screen.dart';
import 'screens/math_screen.dart';
import 'screens/number_screen.dart';
import 'screens/random_screen.dart';
import 'screens/year_screen.dart';

void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FactBloc(),
      child: MaterialApp(
        title: 'Interesting Facts',
        theme: ThemeData(
          primarySwatch: Colors.amber,
          primaryColor: Color(0xFFFEDB77),
          accentColor: Colors.deepPurple[400],
        ),
        initialRoute: RandomScreen.name,
        routes: {
          RandomScreen.name: (context) => RandomScreen(),
          NumberScreen.name: (context) => NumberScreen(),
          MathScreen.name: (context) => MathScreen(),
          DateScreen.name: (context) => DateScreen(),
          YearScreen.name: (context) => YearScreen(),
        },
      ),
    );
  }
}
