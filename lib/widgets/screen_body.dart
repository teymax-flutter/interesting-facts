import 'package:flutter/material.dart';

import 'widgets.dart';

class ScreenBody extends StatelessWidget {
  const ScreenBody({
    Key key,
    @required this.factType,
  }) : super(key: key);

  final String factType;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 10),
            MainDisplay(),
            SizedBox(height: 20),
            FactControls(factType: factType),
          ],
        ),
      ),
    );
  }
}
