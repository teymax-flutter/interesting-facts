import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/fact_bloc.dart';
import '../bloc/fact_event.dart';

class FactControls extends StatefulWidget {
  final String factType;
  FactControls({@required this.factType});

  @override
  _FactControlsState createState() => _FactControlsState();
}

class _FactControlsState extends State<FactControls> {
  TextEditingController _controller = TextEditingController();
  String _input = '';

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: _controller,
          keyboardType: TextInputType.number,
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: 30),
          decoration: const InputDecoration(
            border: OutlineInputBorder(),
            hintText: 'Input a number',
          ),
          onChanged: (value) {
            _input = value;
          },
          onSubmitted: (_) {
            loadConcrete();
          },
        ),
        SizedBox(height: 10),
        Row(
          children: [
            Expanded(
              child: RaisedButton(
                padding: const EdgeInsets.all(20),
                child: const Text(
                  'Search',
                  style: TextStyle(fontSize: 20),
                ),
                onPressed: loadConcrete,
                color: Theme.of(context).primaryColor,
                textTheme: ButtonTextTheme.primary,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: RaisedButton(
                padding: const EdgeInsets.all(20),
                child: const Text(
                  'Random',
                  style: const TextStyle(fontSize: 20),
                ),
                onPressed: loadRandom,
                color: Theme.of(context).accentColor,
                textTheme: ButtonTextTheme.primary,
              ),
            ),
          ],
        ),
      ],
    );
  }

  void loadConcrete() {
    BlocProvider.of<FactBloc>(context).add(
      FactLoadEvent(
        factType: widget.factType,
        number: _input,
      ),
    );
    _controller.clear();
  }

  void loadRandom() {
    BlocProvider.of<FactBloc>(context).add(
      FactLoadEvent(factType: widget.factType),
    );
    _controller.clear();
  }
}
