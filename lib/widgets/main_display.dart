import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/fact_bloc.dart';
import '../bloc/fact_state.dart';
import 'widgets.dart';

class MainDisplay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FactBloc, FactState>(
      builder: (context, state) {
        switch (state.runtimeType) {
          case FactEmptyState:
            return MessageDisplay(message: 'Start searching!');
            break;

          case FactLoadingState:
            return LoadingWidget();
            break;

          case FactLoadedState:
            return FactDisplay(fact: (state as FactLoadedState).loadedFact);
            break;

          case FactErrorState:
            return MessageDisplay(
                message: (state as FactErrorState)
                        .errorMessage
                        .startsWith('Exception: SocketException')
                    ? 'Could not load a fact. Please check your internet connection. If it\'s fine then we messed up somewhere :)'
                    : (state as FactErrorState)
                            .errorMessage
                            .startsWith('FormatException')
                        ? 'Invalid input format. Only integer numbers are supported.'
                        : (state as FactErrorState).errorMessage);
            break;

          default:
            return MessageDisplay(message: 'Unexpected outcome...');
            break;
        }
      },
    );
  }
}
