import 'package:flutter/material.dart';
import 'package:interesting_facts/models/date_fact.dart';
import 'package:interesting_facts/models/year_fact.dart';

import '../models/number_fact.dart';

class FactDisplay extends StatelessWidget {
  final NumberFact fact;

  const FactDisplay({@required this.fact});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 2,
      child: Column(
        children: [
          Text(
            fact.number.toString(),
            style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
          ),
          if (fact is DateFact)
            Text(
              '${(fact as DateFact).year.toString()} year',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          if (fact is YearFact)
            Text(
              (fact as YearFact).date,
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
          Expanded(
            child: Center(
              child: SingleChildScrollView(
                child: Text(
                  fact.text,
                  style: TextStyle(fontSize: 25),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
