import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/fact_bloc.dart';
import '../bloc/fact_event.dart';

class DrawerItem extends StatelessWidget {
  final String text;
  final bool isCurrent;
  final String navigateTo;
  DrawerItem(
      {@required this.text, @required this.navigateTo, this.isCurrent = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      child: FlatButton(
        onPressed: () {
          BlocProvider.of<FactBloc>(context).add(FactClearEvent());
          Navigator.of(context).pushReplacementNamed(navigateTo);
        },
        child: Text(
          text,
          style: TextStyle(
            fontSize: 22,
            color: isCurrent ? Colors.yellowAccent : Colors.white,
          ),
        ),
        color: isCurrent ? Colors.deepPurple : Theme.of(context).accentColor,
        padding: const EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      ),
    );
  }
}
