import 'package:flutter/material.dart';

import '../screens/date_screen.dart';
import '../screens/math_screen.dart';
import '../screens/number_screen.dart';
import '../screens/random_screen.dart';
import '../screens/year_screen.dart';
import 'drawer_item.dart';

class CustomDrawer extends StatelessWidget {
  final String currentScreen;
  final List<Map<String, dynamic>> itemsData = [
    {
      'text': 'Random facts',
      'route': RandomScreen.name,
    },
    {
      'text': 'Number facts',
      'route': NumberScreen.name,
    },
    {
      'text': 'Mathematical facts',
      'route': MathScreen.name,
    },
    {
      'text': 'Facts about date',
      'route': DateScreen.name,
    },
    {
      'text': 'Facts about year',
      'route': YearScreen.name,
    }
  ];

  CustomDrawer({@required this.currentScreen});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        padding: const EdgeInsets.all(10),
        color: Theme.of(context).primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: buildItems(),
        ),
      ),
    );
  }

  List<DrawerItem> buildItems() {
    List<DrawerItem> result = [];

    for (Map<String, dynamic> item in itemsData) {
      result.add(
        DrawerItem(
          text: item['text'],
          navigateTo: item['route'],
          isCurrent: currentScreen == item['route'] ? true : false,
        ),
      );
    }

    return result;
  }
}
