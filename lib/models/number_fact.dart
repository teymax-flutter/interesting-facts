import 'package:flutter/material.dart';

class NumberFact {
  final num number;
  final String text;

  NumberFact({@required this.number, @required this.text});

  factory NumberFact.fromJson(Map<String, dynamic> json) {
    return NumberFact(number: json['number'], text: json['text']);
  }
}
