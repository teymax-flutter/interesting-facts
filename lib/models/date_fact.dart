import 'package:flutter/material.dart';
import 'number_fact.dart';

class DateFact extends NumberFact {
  final int year;

  DateFact({@required number, @required text, @required this.year})
      : super(number: number, text: text);

  factory DateFact.fromJson(Map<String, dynamic> json) {
    return DateFact(
      number: json['number'],
      text: json['text'],
      year: json['year'],
    );
  }
}
