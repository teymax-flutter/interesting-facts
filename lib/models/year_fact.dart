import 'package:flutter/material.dart';
import 'number_fact.dart';

class YearFact extends NumberFact {
  final String date;

  YearFact({@required number, @required text, @required this.date})
      : super(number: number, text: text);

  factory YearFact.fromJson(Map<String, dynamic> json) {
    return YearFact(
      number: json['number'],
      text: json['text'],
      date: json['date'] ?? '',
    );
  }
}
