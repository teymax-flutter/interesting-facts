import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/number_fact.dart';
import '../models/date_fact.dart';
import '../models/year_fact.dart';

class ApiProvider {
  Future<NumberFact> getFact(String number, String factType) async {
    String apiUrl = 'http://numbersapi.com/$number/$factType?json=true';
    final client = http.Client();
    try {
      var uriResponse = await client.get(apiUrl);
      final Map<String, dynamic> factJson = json.decode(uriResponse.body);

      return _getResult(factType, factJson);
    } catch (error) {
      print(error);
      throw Exception(error.toString());
    } finally {
      client.close();
    }
  }

  Future<NumberFact> getRandomFact(String factType) async {
    String apiUrl = 'http://numbersapi.com/random/$factType?json=true';
    final client = http.Client();
    try {
      var uriResponse = await client.get(apiUrl);
      final Map<String, dynamic> randomFactJson = json.decode(uriResponse.body);

      return _getResult(factType, randomFactJson);
    } catch (error) {
      print(error);
      throw Exception(error.toString());
    } finally {
      client.close();
    }
  }

  NumberFact _getResult(String factType, Map<String, dynamic> factJson) {
    NumberFact result;

    switch (factType) {
      case 'trivia':
        result = NumberFact.fromJson(factJson);
        break;
      case 'math':
        result = NumberFact.fromJson(factJson);
        break;
      case 'date':
        result = DateFact.fromJson(factJson);
        break;
      case 'year':
        result = YearFact.fromJson(factJson);
        break;
    }

    return result;
  }
}
