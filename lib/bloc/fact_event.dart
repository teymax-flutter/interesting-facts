import 'package:flutter/foundation.dart';

abstract class FactEvent {}

class FactLoadEvent extends FactEvent {
  final String number;
  final String factType;
  FactLoadEvent({this.number, @required this.factType});
}

class RandomFactLoadEvent extends FactEvent {}

class FactClearEvent extends FactEvent {}
