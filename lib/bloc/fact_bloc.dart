import 'dart:math';

import 'package:flutter_bloc/flutter_bloc.dart';

import '../services/api_provider.dart';
import '../models/number_fact.dart';
import 'fact_event.dart';
import 'fact_state.dart';

class FactBloc extends Bloc<FactEvent, FactState> {
  final ApiProvider _apiProvider = ApiProvider();

  FactBloc() : super(FactEmptyState());

  @override
  Stream<FactState> mapEventToState(FactEvent event) async* {
    try {
      if (event is FactLoadEvent) {
        yield FactLoadingState();

        if (event.number != null && event.number.isEmpty) {
          yield FactErrorState(errorMessage: 'Enter a number!');
        } else {
          String number = event.number == null
              ? 'random'
              : int.parse(event.number).toString();
          NumberFact _loadedFact =
              await _apiProvider.getFact(number, event.factType);

          yield FactLoadedState(loadedFact: _loadedFact);
        }
      } else if (event is RandomFactLoadEvent) {
        yield FactLoadingState();
        List<String> types = ['trivia', 'math', 'date', 'year'];

        NumberFact _loadedFact =
            await _apiProvider.getRandomFact(types[Random().nextInt(4)]);

        yield FactLoadedState(loadedFact: _loadedFact);
      } else if (event is FactClearEvent) {
        yield FactEmptyState();
      }
    } catch (error) {
      yield FactErrorState(errorMessage: error.toString());
    }
  }
}
