import 'package:flutter/foundation.dart';

import '../models/number_fact.dart';

abstract class FactState {}

class FactEmptyState extends FactState {}

class FactLoadingState extends FactState {}

class FactLoadedState extends FactState {
  NumberFact loadedFact;

  FactLoadedState({@required this.loadedFact}) : assert(loadedFact != null);
}

class FactErrorState extends FactState {
  String errorMessage;

  FactErrorState({@required this.errorMessage}) : assert(errorMessage != null);
}
