import 'package:flutter_test/flutter_test.dart';
import 'package:interesting_facts/models/number_fact.dart';
import 'package:interesting_facts/services/api_provider.dart';

void main() {
  test('ApiProvider().getFact() should return NumberFact', () async {
    var fact = await ApiProvider().getFact('1', 'trivia');
    expect(fact.runtimeType, NumberFact);
    expect(fact.number, 1);
    expect(fact.text.runtimeType, String);
  });
}
